# Csharp_EmptyRecycleBin

A  C# class library to easy empty the recycle bin



##Getting Started
Download one of the [releases](https://github.com/Hoog3059/Csharp_EmptyRecycleBin/releases)
Put it in your C# project as reference

###Usage
Don't forget to put:
<pre>
using EmptyBin;
</pre>

In your C# code put:
<pre>
EmptyBin.SHEmptyRecycleBin(IntPtr.Zero, null, EmptyBin.RecycleFlags.Flag);
</pre>

####Other Info
Csharp_EmptyRecycleBin is under MIT license

#####Copyright (c) 2015 Hoog3059
